﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonDoTween : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] Text txtUpEase, txtDownEase;
    [SerializeField] RectTransform rtBody;

    Ease downEase;
    Ease upEase;

    float downScale;
    float downDuration;
    float upDuration;

    public ButtonDoTween SetTextUpEase(string text)
    {
        txtUpEase.text = text;
        return this;
    }

    public ButtonDoTween SetTextDownEase(string text)
    {
        txtDownEase.text = text;
        return this;
    }

    public ButtonDoTween SetDownEase(Ease ease)
    {
        downEase = ease;
        return this;
    }

    public ButtonDoTween SetUpEase(Ease ease)
    {
        upEase = ease;
        return this;
    }

    public ButtonDoTween SetDownScaleValue(double scaleValue)
    {
        downScale = (float)scaleValue;
        return this;
    }    

    public ButtonDoTween SetDownDuration(double duration)
    {
        downDuration = (float)duration;
        return this;
    }

    public ButtonDoTween SetUpDuration(double duration)
    {
        upDuration = (float)duration;
        return this;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        rtBody.DOKill();
        rtBody.localScale = Vector3.one;
        rtBody.DOScale(downScale, downDuration).SetEase(downEase).OnComplete(() =>
        {
            rtBody.DOScale(1f, upDuration).SetEase(upEase);
        });
    }
}
