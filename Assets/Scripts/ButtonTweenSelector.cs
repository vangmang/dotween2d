﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class ButtonTweenSelector : MonoBehaviour
{
    [SerializeField] ButtonDoTween btnDotween;
    Vector2 upEaseScrollPosition, downEaseScrollPosition;
    string strDownDuration, strUpDuration, strDownScale;

    private void Start()
    {
        strDownDuration = "1";
        strUpDuration = "1";
        strDownScale = "0.95";
        btnDotween.SetUpEase(Ease.Unset)
                  .SetTextUpEase($"<size=20>upEase</size>\n<color=#120A52>{Ease.Unset.ToString()}</color>")
                  .SetDownEase(Ease.Unset)
                  .SetTextDownEase($"<size=20>downEase</size>\n<color=#120A52>{Ease.Unset.ToString()}</color>");
    }

    public void OnGUI()
    {
        GUI.skin.button.fontSize = 24;
        GUI.skin.textField.fontSize = 24;
        GUI.skin.textArea.fontSize = 20;
        GUI.skin.textArea.alignment = TextAnchor.MiddleCenter;

        var easeArr = Enum.GetValues(typeof(Ease));
        var count = easeArr.Length - 2;

        GUI.TextArea(new Rect(10f, Screen.height * 0.5f - 440f, 170f, 30f), "UP");
        // UpEase
        upEaseScrollPosition = GUI.BeginScrollView(new Rect(10f, Screen.height * 0.5f - 400f, 200f, 800f), upEaseScrollPosition, new Rect(0f, 0f, 180f, count * 60f));
        
        for (int i = 0; i < count; i++)
        {
            var targetEase = (Ease)easeArr.GetValue(i);
            if (GUI.Button(new Rect(0f, i * 60f, 180f, 50f), targetEase.ToString()))
                btnDotween.SetUpEase(targetEase)
                          .SetTextUpEase($"<size=20>upEase</size>\n<color=#120A52>{targetEase.ToString()}</color>");
        }

        GUI.EndScrollView();

        GUI.TextArea(new Rect(Screen.width - 180f, Screen.height * 0.5f - 440f, 170f, 30f), "Down");
        // DownEase
        downEaseScrollPosition = GUI.BeginScrollView(new Rect(Screen.width - 210f, Screen.height * 0.5f - 400f, 200f, 800f), downEaseScrollPosition, new Rect(Screen.width - 210f, 0f, 180f, count * 60f));

        for (int i = 0; i < count; i++)
        {
            var targetEase = (Ease)easeArr.GetValue(i);
            if (GUI.Button(new Rect(Screen.width - 210f, i * 60f, 180f, 50f), targetEase.ToString()))
                btnDotween.SetDownEase(targetEase)
                          .SetTextDownEase($"<size=20>downEase</size>\n<color=#120A52>{targetEase.ToString()}</color>");
        }

        GUI.EndScrollView();


        GUI.TextArea(new Rect(10f, Screen.height - 200f, 170f, 30f), "Duration - Down");
        strDownDuration = GUI.TextField(new Rect(190f, Screen.height - 200f, 150, 30), strDownDuration, 30);
        double.TryParse(strDownDuration, out double downDuration);

        GUI.TextArea(new Rect(10f, Screen.height - 160f, 170f, 30f), "Duration - up");
        strUpDuration = GUI.TextField(new Rect(190f, Screen.height - 160f, 150, 30), strUpDuration, 30);
        double.TryParse(strUpDuration, out double upDuration);

        GUI.TextArea(new Rect(10f, Screen.height - 120f, 170f, 30f), "PressedScale");
        strDownScale = GUI.TextField(new Rect(190f, Screen.height - 120f, 150, 30), strDownScale, 30);
        double.TryParse(strDownScale, out double downScale);

        btnDotween.SetDownDuration(downDuration)
                  .SetUpDuration(upDuration)
                  .SetDownScaleValue(downScale);
    }

}
